package no.ntnu.imt3281.sudoku;

import static org.junit.Assert.*;

import org.junit.Test;

public class BadNumberExceptionTest {

	@Test
	public void toStringTest() {
		BadNumberException bne = new BadNumberException("Error", 1, 2, BadNumberException.COLLISIONONCOLUMN);
		assertEquals("Collision in column 2 row 1", bne.toString());
		
		bne = new BadNumberException("Error", 3, 2, BadNumberException.COLLISIONONROW);
		assertEquals("Collision on row 3 column 2", bne.toString());
		
		bne = new BadNumberException("Error", 3, 2, BadNumberException.COLLISIONONSUBGRID);
		assertEquals("Collision in subgrid 2 position 3", bne.toString());
	}

}
