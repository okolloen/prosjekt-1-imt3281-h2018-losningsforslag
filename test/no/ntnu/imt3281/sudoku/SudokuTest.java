package no.ntnu.imt3281.sudoku;

import static org.junit.Assert.*;

import java.util.Iterator;
import static org.hamcrest.Matchers.is;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SudokuTest {
	private Sudoku sudoku;
	private String json = "[[5, 3, -1, -1, 7, -1, -1, -1, -1],[6, -1, -1, 1, 9, 5, -1, -1, -1], [-1, 9, 8, -1, -1, -1, -1, 6, -1], [8, -1, -1, -1, 6, -1, -1, -1, 3], [4, -1, -1, 8, -1, 3, -1, -1, 1], [7, -1, -1, -1, 2, -1, -1, -1, 6], [-1, 6, -1, -1, -1, -1, 2, 8, -1], [-1, -1, -1, 4, 1, 9, -1, -1, 5], [-1, -1, -1, -1, 8, -1, -1, 7, 9]]";
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Before
	public void setup() {
		sudoku = new Sudoku();
	}
	
	@Test
	public void testEmptyConstructor() {
		assertTrue(sudoku instanceof Sudoku);
	}
	
	@Test
	public void testSetGetValueOfCell() {
		assertEquals(-1, sudoku.getCell(0));	// Cells should be -1 initially
		sudoku.setCell(0, 1);
		assertEquals(1, sudoku.getCell(0));		// Cell should now be 1
		
		sudoku.setCell(9*9-1, 42);
		assertEquals(42, sudoku.getCell(9*9-1)); 
	}
	
	@Test
	public void testSetValueRowCol() {
		sudoku.set(2, 5, 1);					// Set row 2, column 5 to 1
		assertEquals(1, sudoku.getCell(17));	// This is cell 17
		sudoku.set(7, 7, 9);
		assertEquals(9, sudoku.getCell(76));
	}
	
	@Test 
	public void testParseJSONData() {
		sudoku.parseJSON(json);
		assertEquals(6, sudoku.getCell(3));		// Second row, first column
		assertEquals(9, sudoku.getCell(80));		// Last row, last column
	}
	
	@Test
	public void testGetRow() {
		sudoku.parseJSON(json);
		Iterator<Integer> it = sudoku.getRow(0);	// Get first row
		assertTrue(it.hasNext());
		int val = it.next();
		assertEquals(5, val);
		val = it.next();
		assertEquals(3, val);
		for (int i=2; i<8; i++) {
			it.next();
		}
		assertFalse(it.hasNext());
		
		it = sudoku.getRow(0);	// 5, 3, -1, -1, 7, -1, -1, -1, -1
		int[] row = sudoku.iteratorToIntArray(it);
		int[] firstRow = { 5, 3, -1, -1, 7, -1, -1, -1 };
		assertArrayEquals(firstRow, row);
	}
	
	@Test
	public void testGetCol() {
		sudoku.parseJSON(json);
		Iterator<Integer> it = sudoku.getCol(10);
		assertEquals(null, it);	// No iterator for column 10
		it = sudoku.getCol(1);	// Get second column
		assertTrue(it.hasNext());
		int val = it.next();
		assertEquals(3, val);
		val = it.next();
		assertEquals(-1, val);
		for (int i=2; i<8; i++) {
			it.next();
		}
		assertFalse(it.hasNext());
	}
	
	@Test
	public void testGetSubGrid() {
		sudoku.parseJSON(json);
		Iterator<Integer> it = sudoku.getSubGrid(4);	// Get first row
		assertTrue(it.hasNext());
		int val = it.next();
		assertEquals(-1, val);
		val = it.next();
		assertEquals(6, val);
		it.next();
		val = it.next();
		assertEquals(8, val);
		for (int i=2; i<6; i++) {
			it.next();
		}
		assertFalse(it.hasNext());
	}
	
	@Test
	public void testOnlyAllowValidValues() {
		exception.expectMessage("Number given not in range (1-9)");
		sudoku.set(4, 2, 0);
	}
	
	@Test
	public void testOnlyAllowValidRowColumn() {
		exception.expectMessage("Invalid row/column");
		sudoku.set(10, 2, 6);
	}
	
	@Test
	public void testNumberExistsInColumn() {
		sudoku.parseJSON(json);
		sudoku.set(4, 2, 6);		// Valid number
		Exception e = new BadNumberException ("Number already exists in column", 0, 4, BadNumberException.COLLISIONONCOLUMN);
		exception.expect(is(e));
		sudoku.set(0, 6, 7);		// Invalid, 7 is already in column 4
	}
	
	@Test
	public void testNumberExistsInRow() {
		sudoku.parseJSON(json);
		sudoku.set(4, 2, 6);		// Valid number
		Exception e = new BadNumberException ("Number already exists on row", 6, 7, BadNumberException.COLLISIONONROW);
		exception.expect(is(e));
		sudoku.set(5, 7, 8);		// Invalid, 8 is already on row 6
	}
	
	@Test
	public void testNumberExistsInSubGrid() {
		sudoku.parseJSON(json);
		sudoku.set(4, 2, 6);		// Valid number
		Exception e = new BadNumberException ("Number already exists in subgrid", 4, 5, BadNumberException.COLLISIONONSUBGRID);
		exception.expect(is(e));
		sudoku.set(5, 3, 3);		// Invalid, 2 is already in subgrid 4, position 5
	}
	
	@Test
	public void testMirrorBoard() {
		sudoku.parseJSON(json);
		sudoku.mirror();
		assertEquals(6, sudoku.getCell(23));
		assertEquals(3, sudoku.getCell(39));
		assertEquals(1, sudoku.getCell(67));
	}
	
	@Test
	public void testFlippedBoard() {
		sudoku.parseJSON(json);
		sudoku.flip();
		assertEquals(3, sudoku.getCell(61));
		assertEquals(3, sudoku.getCell(41));	
		assertEquals(5, sudoku.getCell(23));
	}
	
	@Test
	public void testMirrorNE2SW() {
		sudoku.parseJSON(json);
		sudoku.mirrorNE2SW();
		assertEquals(5, sudoku.getCell(80));
		assertEquals(9, sudoku.getCell(0));
		assertEquals(2, sudoku.getCell(39));
		assertEquals(1, sudoku.getCell(10));
	}
	
	@Test
	public void testMirrorNW2SE() {
		sudoku.parseJSON(json);
		sudoku.mirrorNW2SE();
		assertEquals(7, sudoku.getCell(11));
		assertEquals(8, sudoku.getCell(8));
		assertEquals(3, sudoku.getCell(69));
		assertEquals(6, sudoku.getCell(1));
	}
	
	@Test
	public void testRandomizeBoard() {
		sudoku.parseJSON(json);
		int prevOne = sudoku.getCell(12);
		int prevTwo = sudoku.getCell(43);
		int prevThree = sudoku.getCell(1);
		int prevFour = sudoku.getCell(30);
		sudoku.randomize();
		assertFalse("Four numbers should not be the same after randomization", 
				prevOne==sudoku.getCell(12)&&prevTwo==sudoku.getCell(43)&&
				prevThree==sudoku.getCell(1)&&prevFour==sudoku.getCell(30));
		assertEquals(sudoku.getCell(0), sudoku.getCell(14));
		assertEquals(sudoku.getCell(27), sudoku.getCell(73));
	}
	
	@Test
	public void testLockingOfBoard() {
		sudoku.parseJSON(json);
		sudoku.lock();
		sudoku.set(0, 2, 1); 	// This is legal
		sudoku.set(0, 2, 2);	// This is legal
		exception.expectMessage("This element is locked");
		sudoku.set(0, 1, 2); 	// Not legal, cell is locked
	}
	
	@Test
	public void testIsLocked() {
		sudoku.parseJSON(json);
		sudoku.lock();
		assertFalse(sudoku.isLocked(0,2));
		assertFalse(sudoku.isLocked(8,0));
		assertFalse(sudoku.isLocked(4,4));
		assertTrue(sudoku.isLocked(0,0));
		assertTrue(sudoku.isLocked(8,8));
		assertTrue(sudoku.isLocked(4,3));
	}
	
	@Test
	public void testGetRowCol() {
		sudoku.parseJSON(json);
		assertEquals(9, sudoku.get(2, 1));
		assertEquals(8, sudoku.get(8, 4));
	}
}
