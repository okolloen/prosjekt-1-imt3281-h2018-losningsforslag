package no.ntnu.imt3281.sudoku;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class SudokuController {
	private TextField[][] board = new TextField[9][9];
    @FXML
    private GridPane sudokuBoard;
    Sudoku sudoku = null;

    @FXML
    void newGame(ActionEvent event) {
    	sudoku = new Sudoku();
    	String json = "[[5, 3, -1, -1, 7, -1, -1, -1, -1],[6, -1, -1, 1, 9, 5, -1, -1, -1], [-1, 9, 8, -1, -1, -1, -1, 6, -1], [8, -1, -1, -1, 6, -1, -1, -1, 3], [4, -1, -1, 8, -1, 3, -1, -1, 1], [7, -1, -1, -1, 2, -1, -1, -1, 6], [-1, 6, -1, -1, -1, -1, 2, 8, -1], [-1, -1, -1, 4, 1, 9, -1, -1, 5], [-1, -1, -1, -1, 8, -1, -1, 7, 9]]";
    	sudoku.parseJSON(json);
    	if (Math.random()>0.5) {
    		sudoku.mirror();
    	}
    	if (Math.random()>0.5) {
    		sudoku.flip();
    	}
    	if (Math.random()>0.5) {
    		sudoku.mirrorNE2SW();
    	}
    	if (Math.random()>0.5) {
    		sudoku.mirrorNW2SE();
    	}
    	sudoku.randomize();
    	sudoku.lock();
    	for (int row=0; row<9; row++) {
    		for (int col=0; col<9; col++) {
    			if (sudoku.isLocked(row, col)) {
    				board[row][col].setText(sudoku.get(row, col)+"");
    				board[row][col].setEditable(false);
    				board[row][col].setStyle("-fx-background-color: #BBB");
    			} else {
    				board[row][col].setText("");
    				board[row][col].setEditable(true);
    				board[row][col].setStyle("");
    			}
    		}
    	}
    }
    
    @FXML
    void newMediumGame(ActionEvent event) {
    	// To be added at a later stage
    }
    
    @FXML
    void initialize() {
    	GridPane grid=null;
    	for (int subgridy=0; subgridy<3; subgridy++) {
	    	for (int subgridx=0; subgridx<3; subgridx++) {
	    		grid = new GridPane();
	        	grid.setAlignment(Pos.CENTER);
	        	grid.setHgap(10);
	        	grid.setVgap(10);
	        	grid.setPadding(new Insets(4, 4, 4, 4));
	        	for (int i=0; i<9; i++) {
	    	    	board[i%3+subgridy*3][i/3+subgridx*3] = new TextField();
	    	    	board[i%3+subgridy*3][i/3+subgridx*3].setPrefWidth(35);
	    	    	board[i%3+subgridy*3][i/3+subgridx*3].setAlignment(Pos.CENTER);
	    	    	final int row = i%3+subgridy*3;
	    	    	final int col = i/3+subgridx*3;
	    	    	board[i%3+subgridy*3][i/3+subgridx*3].textProperty().addListener((observable, oldValue, newValue)-> 
	    	    		cellValueChanged(row, col, newValue)
	    	    	);
	    	    	grid.add(board[i%3+subgridy*3][i/3+subgridx*3], i/3, i%3);
	        	}
	        	grid.setStyle("-fx-border-color: black;");
	        	sudokuBoard.add(grid, subgridx, subgridy);
	    	}
    	}
    }

    /**
     * Called when the value of a cell has changed
     * 
     * @param row the row at which the value changed
     * @param col the column at which the value changed
     * @param newValue the new value at this row/column
     */
	private void cellValueChanged(final int row, final int col, String newValue) {
		final TextField toSelect = board[row][col];
		try {
			int t = Integer.parseInt(newValue);
			if (t<1||t>9) {
				Platform.runLater(()-> {
					toSelect.setText("");
					board[row][col].setStyle("");
					sudoku.set(row, col, -1);
				});
			} else {
				setNewCellValue(row, col, t);
			}
		} catch (RuntimeException re) {
			Platform.runLater(()-> {
				toSelect.setText("");
				board[row][col].setStyle("");
				sudoku.set(row, col, -1);
			});
		}
		Platform.runLater(()-> 
			toSelect.selectAll()
		);
	}

	private void setNewCellValue(final int row, final int col, int t) {
		try {
			sudoku.set(row, col, t);
			board[row][col].setStyle("");
		} catch (RuntimeException e) {
			board[row][col].setStyle("-fx-background-color: #D22");
		}
	}
}
