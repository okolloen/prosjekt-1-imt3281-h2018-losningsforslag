/**
 * 
 */
package no.ntnu.imt3281.sudoku;

/**
 * @author oeivindk
 *
 */
public class BadNumberException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int row;
	private final int col;
	private final int type;
	
	public static final int COLLISIONONROW = 1;
	public static final int COLLISIONONCOLUMN = 2;
	public static final int COLLISIONONSUBGRID = 4;

	/**
	 * Create a new BadNumberException object, give a textual reason, the row 
	 * and column that already contains the number and if this is on the same
	 * row/column/sub-grid.
	 * 
	 * @param reason a textual explanation of what caused the exception
	 * @param row on what row does the number exist
	 * @param col on what column does the number exists
	 * @param type does the number exist on the same row/column and or sub-grid
	 */
	public BadNumberException(String reason, int row, int col, int type) {
		super(reason);
		this.row = row;
		this.col = col;
		this.type = type;
	}

	@Override
	public String toString() {
		if ((type & COLLISIONONROW) > 0) {
			return "Collision on row "+row+" column "+col;
		}
		if ((type & COLLISIONONCOLUMN) > 0) {
			return "Collision in column "+col+ " row "+row;
		}
		if ((type & COLLISIONONSUBGRID) > 0) {
			return "Collision in subgrid "+col+" position "+row; 
		}
		return "";
	}
	
	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return the col
	 */
	public int getCol() {
		return col;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof BadNumberException && getMessage().equals(((BadNumberException)o).getMessage())&&
				row==((BadNumberException)o).getRow()&&
				col==((BadNumberException)o).getCol()&&
				type==((BadNumberException)o).getType());
	}
	
	@Override 
	public int hashCode() {
		return super.hashCode();
	}
}
