package no.ntnu.imt3281.sudoku;

public class LockedElementException extends RuntimeException {

	public LockedElementException(String msg) {
		super(msg);
	}

}
