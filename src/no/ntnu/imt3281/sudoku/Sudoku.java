package no.ntnu.imt3281.sudoku;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import org.json.JSONArray;

/**
 * An object of this class will represent a sudoku board.
 * The class provides methods to access and set the cells of the board in different ways.
 * 
 * The class also contains methods to manipulate the board in different ways (flip, mirror, exchange numbers, check for validity.)
 * 
 * @author oeivindk
 *
 */
public class Sudoku {
	private static final int GRIDSIZE = 9*9;
	private static final int EMPTY = -1;
	private static final String NO_MORE_ITEMS = "No more items";
	private int[] grid = new int[GRIDSIZE];
	private boolean[][] locks = new boolean[9][9]; 

	
	/**
	 * Empty constructor, initialize all cells to -1 (empty cell)
	 */
	public Sudoku () {
		for (int i=0; i<GRIDSIZE; i++) {	// Set all cells of grid to -1 (empty cell)
			grid[i] = EMPTY;
		}
		for (int row=0; row<9; row++) {
			for (int col=0; col<9; col++) {
				locks[row][col] = false;
			}
		}
	}
	
	/**
	 * Set the value of a single cell to a given value. Not intended for general use but used internally 
	 * and for testing purpose.
	 * 
	 * @param cell the index of the cell to set
	 * @param value the value to store in this cell
	 */
	protected void setCell(int cell, int value) {
		grid[cell] = value;
	}
	
	/**
	 * Get the value of a single cell. Not intended for general use but used internally 
	 * and for testing purpose.
	 * 
	 * @param cell the index of the cell to get
	 * @return int the value of this cell
	 */
	protected int getCell(int cell) {
		return grid[cell];
	}

	/**
	 * Set the value of a single cell at a given row and column.
	 * 
	 * @param row the row containing the cell to set the value of
	 * @param col the column containing the cell to set the value of
	 * @param value the value to set
	 */
	public void set(int row, int col, int value) {
		if ((value<1||value>9)&&value!=-1) {
			throw new ArrayIndexOutOfBoundsException("Number given not in range (1-9)");
		}
		if (row<0||row>8||col<0||col>8) {
			throw new ArrayIndexOutOfBoundsException("Invalid row/column");
		}
		if (locks[row][col]) {
			throw new LockedElementException("This element is locked");
		}
		if (value>-1) {
			int subgridCollision = -1;
			int colCollision = checkForColumnCollision(row, value);
			int rowCollision = checkForRowCollision(col, value);
			subgridCollision = checkForSubGridCollision(row, col, value);
			// TODO : Complete subgrid collision code.
			if (rowCollision>-1&&colCollision>-1) {
				throw new BadNumberException("Number already exists on row/column", rowCollision, colCollision, BadNumberException.COLLISIONONROW+BadNumberException.COLLISIONONCOLUMN);
			} else if (rowCollision>-1) {
				throw new BadNumberException("Number already exists on row", rowCollision, col, BadNumberException.COLLISIONONROW);
			} else if (colCollision>-1) {
				throw new BadNumberException("Number already exists in column", row, colCollision, BadNumberException.COLLISIONONCOLUMN);
			} else if (subgridCollision>-1) {
				int subgrid = (row%3*3+row/3*27+col%3+col/3*9)/9;
				throw new BadNumberException("Number already exists in subgrid", subgrid, subgridCollision, BadNumberException.COLLISIONONSUBGRID);
			}
		}
		grid[row%3*3+row/3*27+col%3+col/3*9] = value;
	}

	private int checkForSubGridCollision(int row, int col, int value) {
		int subgridCollision = -1;
		Iterator<Integer> it;
		it = getSubGrid((row%3*3+row/3*27+col%3+col/3*9)/9);
		for (int i=0; i<8; i++) {
			if ((int)it.next()==value) {
				subgridCollision = i;
			}
		}
		return subgridCollision;
	}

	private int checkForRowCollision(int col, int value) {
		int rowCollision = -1;
		Iterator<Integer> it;
		it = getCol(col);
		for (int i=0; i<8; i++) {
			if ((int)it.next()==value) {
				rowCollision = i;
			}
		}
		return rowCollision;
	}

	private int checkForColumnCollision(int row, int value) {
		int colCollision = -1;
		Iterator<Integer> it = getRow(row);
		for (int i=0; i<8; i++) {
			if (it.next()==value) {
				colCollision = i;
			}
		}
		return colCollision;
	}
	
	/**
	 * Convenience method to get the value at a given row/column.
	 * 
	 * @param row
	 * @param col
	 * @return the value at the given row/column.
	 */
	public int get (int row, int col) {
		return grid[row%3*3+row/3*27+col%3+col/3*9];
	}

	/**
	 * Parses a JSON string containing information about a sudoku
	 * board, contains an array where each elements is another array
	 * with the values for each cell in each row. The first element
	 * is the first row, then the first element in that array is the
	 * first cell in that row.
	 *  
	 * @param json A string with json encoded rows and cells.
	 */
	public void parseJSON(String json) {
		JSONArray rows = new JSONArray (json);
		for (int row=0; row<9; row++) {
			JSONArray values = (JSONArray) rows.get(row);
			for (int col=0; col<9; col++) {
				set (row, col, values.getInt(col));
			}
		}
	}

	/**
	 * Get iterator for given row, will return values for all cells
	 * in a given row.
	 * 
	 * Not intended to be used externally.
	 * 
	 * @param row the row to get iterator for (integer between zero and eight)
	 * @return Iterator for given row
	 */
	protected Iterator<Integer> getRow(int row) {
		if (row<0||row>8) {
			return null;
		}
		return new RowIterator(row);
	}
	
	/**
	 * Get iterator for given column, will return values for all cells
	 * in a given column.
	 * 
	 * Not intended to be used externally.
	 * 
	 * @param row the column to get iterator for (integer between zero and eight)
	 * @return Iterator for given column
	 */
	protected Iterator<Integer> getCol(int col) {
		if (col<0||col>8) {
			return null;
		}
		return new ColIterator(col);
	}

	/**
	 * Get iterator for given sub grid, will return values for all cells
	 * in a given sub grid.
	 * 
	 * Not intended to be used externally.
	 * 
	 * The sub grids are :
	 * | 0 | 1 | 2 |
	 * +---+---+---+
	 * | 3 | 4 | 5 |
	 * +---+---+---+
	 * | 6 | 7 | 8 |
	 * 
	 * Each sub grid contains nine items (three rows, three columns, with the items 
	 * numbered the same way as the grids). 
	 * 
	 * @param gridNr the the index of the sub grid to get iterator for (integer 
	 * between zero and eight)
	 * 
	 * @return Iterator for given grid
	 */
	protected Iterator<Integer> getSubGrid(int gridNr) {
		if (gridNr<0||gridNr>8) {
			return null;
		}
		return new GridIterator(gridNr);
	}

	protected void printGrid() {
		final String boardDisplay = " {0} {1} {2} | {3} {4} {5} | {6} {7} {8}\n";
		Logger logger = Logger.getLogger("BoardDump");

		for (int i=0; i<8; i++) {
			Iterator<Integer> it = getRow(i);
			logger.log(Level.FINE, boardDisplay, iteratorToIntArray(it));
			if (i>0&&i%3==0) {
				logger.log(Level.FINE, "------+------+------+\n");
			}
		}
	}

	protected int[] iteratorToIntArray (Iterator<Integer> it) {
		int[] contents = new int[8];
		for (int i=0; i<8; i++) {
			contents[i] = it.next();
		}
		return contents;
	}
	
	/**
	 * An iterator that will return all values for a given row
	 * in the sudoku board.
	 * 
	 * @author okolloen
	 *
	 */
	private class RowIterator implements Iterator<Integer> {
		private int col=0;
		private int row;
		
		/**
		 * Creates a new iterator, iterating over the values of 
		 * the given row.
		 * 
		 * @param row what row to return the values for
		 */
		public RowIterator(int row) {
			this.row = row;
		}

		@Override
		/**
		 * Checks if there are more values available on this iterator.
		 * 
		 * @return true if more values available, false if no more 
		 * values available.
		 */
		public boolean hasNext() {
			return (col<8);
		}

		@Override
		/**
		 * Return the next value from this iterator.
		 * 
		 * @return the value of the next item on this iterator
		 */
		public Integer next() {
			if (col>=8) {
				throw new NoSuchElementException(NO_MORE_ITEMS);
			}
			return grid[row%3*3+row/3*27+col%3+col++/3*9];
		}
	}

	/**
	 * An iterator that will return all values for a given row
	 * in the sudoku board.
	 * 
	 * @author okolloen
	 *
	 */
	private class ColIterator implements Iterator<Integer> {
		private int row=0;
		private int col;
		
		/**
		 * Creates a new iterator, iterating over the values of 
		 * the given row.
		 * 
		 * @param row what row to return the values for
		 */
		public ColIterator(int col) {
			this.col = col;
		}

		@Override
		/**
		 * Checks if there are more values available on this iterator.
		 * 
		 * @return true if more values available, false if no more 
		 * values available.
		 */
		public boolean hasNext() {
			return (row<8);
		}

		@Override
		/**
		 * Return the next value from this iterator.
		 * 
		 * @return the value of the next item on this iterator
		 */
		public Integer next() {
			if (row>=8) {
				throw new NoSuchElementException(NO_MORE_ITEMS);
			}
			return grid[row%3*3+row++/3*27+col%3+col/3*9];
		}
	}
	
	/**
	 * An iterator that will return all values for a given grid
	 * in the sudoku board.
	 * 
	 * @author okolloen
	 *
	 */
	private class GridIterator implements Iterator<Integer> {
		private int item=0;
		private int grid;
		
		/**
		 * Creates a new iterator, iterating over the values of 
		 * the given grid.
		 * 
		 * @param row what row to return the values for
		 */
		public GridIterator(int grid) {
			this.grid = grid;
		}

		@Override
		/**
		 * Checks if there are more values available on this iterator.
		 * 
		 * @return true if more values available, false if no more 
		 * values available.
		 */
		public boolean hasNext() {
			return (item<8);
		}

		@Override
		/**
		 * Return the next value from this iterator.
		 * 
		 * @return the value of the next item on this iterator
		 */
		public Integer next() {
			if (item>=8) {
				throw new NoSuchElementException(NO_MORE_ITEMS);
			}
			return Sudoku.this.grid[grid*9+item++];
		}
	}

	/**
	 * Mirrors the sudoku board, that is, the first and last column is swapped, the second
	 * and next to last is swapped and so on.
	 */
	protected void mirror() {
		int[] mirrored = grid.clone();
		for (int row=0; row<9; row++) {
			for (int col=0; col<9; col++) {
				int inv = 8-col;
				mirrored[row%3*3+row/3*27+inv%3+inv/3*9] = grid[row%3*3+row/3*27+col%3+col/3*9];
			}
		}
		grid = mirrored;
	}

	/**
	 * Flips the sudoku board upside down, the first and last row is swapped, the second and 
	 * and next to last is swapped and so on.
	 */
	protected void flip() {
		int[] mirrored = grid.clone();
		for (int row=0; row<9; row++) {
			for (int col=0; col<9; col++) {
				int inv = 8-row;
				mirrored[inv%3*3+inv/3*27+col%3+col/3*9] = grid[row%3*3+row/3*27+col%3+col/3*9];
			}
		}
		grid = mirrored;
	}

	/**
	 * Mirrors the board around the line going SE to NW. The top left element is moved down to 
	 * the bottom right and all other items are moved accordingly.
	 */
	protected void mirrorNE2SW() {
		int[] mirrored = grid.clone();
		for (int row=0; row<9; row++) {
			for (int col=0; col<9; col++) {
				int newRow = 8-col;
				int newCol = 8-row;
				mirrored[newRow%3*3+newRow/3*27+newCol%3+newCol/3*9] = grid[row%3*3+row/3*27+col%3+col/3*9];
			}
		}
		grid = mirrored;
	}

	/**
	 * Mirrors the board around the line going NE to SW. The top right element is moved down to
	 * the bottom left and all other items are moved accordingly.
	 */
	protected void mirrorNW2SE() {
		int[] mirrored = grid.clone();
		for (int row=0; row<9; row++) {
			for (int col=0; col<9; col++) {
				int newRow = col;
				int newCol = row;
				mirrored[newRow%3*3+newRow/3*27+newCol%3+newCol/3*9] = grid[row%3*3+row/3*27+col%3+col/3*9];
			}
		}
		grid = mirrored;
	}

	/**
	 * Randomly replaces numbers, that is all ones are randomly replaced by a different number, 
	 * all two's are replaced by a different random number.
	 */
	protected void randomize() {
		int[] numbers = new int[9];
		int number;
		Random random = new Random();
		for (int i=0; i<9; i++) {
			do {
				number = random.nextInt(9)+1;
			} while (contains(numbers, number));
			numbers[i] = number;
		}
		for (int i=0; i<grid.length; i++) {
			if (grid[i]>0) {
				grid[i] = numbers[grid[i]-1];
			}
		}
	}
	
	private static boolean contains(final int[] arr, final int key) {
	    return IntStream.of(arr).anyMatch(x -> x == key);
	}
	
	/**
	 * Locks the board, meaning that elements with values other that -1 can no longer be changed.
	 */
	public void lock() {
		for (int row=0; row<9; row++) {
			for (int col=0; col<9; col++) {
				if (grid[row%3*3+row/3*27+col%3+col/3*9]>0) {
					locks[row][col] = true;
				}
			}
		}
	}

	/**
	 * Checks if the element on a given row/column is locked (part of a given board).
	 * Locked elements are elements that the user can not change.
	 * 
	 * @param row
	 * @param col
	 * @return true if the element is locked, otherwise false
	 */
	public boolean isLocked(int row, int col) {
		return locks[row][col];
	}
}
